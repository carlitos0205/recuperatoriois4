<?php
require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Symfony\Component\HttpFoundation\Request;


$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'pgsql',
    'host'      => 'localhost',
    'database'  => 'tema3.sql',
    'username'  => 'postgres',
    'password'  => 'postgres',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Inicia sesión
session_start();

// Verifica si usuario está autenticado
if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];
    echo "Bienvenido {$usuario['nombre']} {$usuario['apellido']} <br>";
    echo '<a href="logout.php">Cerrar sesión</a>';
} else {
    // Si el formulario se envió
    if (Request::createFromGlobals()->isMethod('post')) {
        $usuario = $_POST['usuario'];
        $contrasena = $_POST['contrasena'];

        // Busca al usuario en la base de datos
        $usuarioDB = Capsule::table('usuarios')
            ->where('usuario', $usuario)
            ->first();

        // Verificar la contraseña
        if ($usuarioDB && password_verify($contrasena, $usuarioDB->contrasena)) {
            // Autenticación exitosa, guardar en sesiones
            $_SESSION['usuario'] = [
                'nombre' => $usuarioDB->nombre,
                'apellido' => $usuarioDB->apellido,
                'usuario' => $usuarioDB->usuario,
            ];

            echo "Bienvenido {$usuarioDB->nombre} {$usuarioDB->apellido} <br>";
            echo '<a href="logout.php">Cerrar sesión</a>';
        } else {
            // Autenticación fallida
            echo "Error: Usuario o contraseña incorrectos. <br>";
            echo '<a href="index.php">Volver al inicio</a>';
        }
    } else {
        // Mostrar el formulario de inicio de sesión
        echo <<<HTML
        <form method="post">
            Usuario: <input type="text" name="usuario" required><br>
            Contraseña: <input type="password" name="contrasena" required><br>
            <input type="submit" value="Iniciar sesión">
        </form>
        HTML;
    }
}
