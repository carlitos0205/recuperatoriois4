<?php
session_start();

// Destruye la sesión
session_destroy();

// Redirige al inicio de sesión
header('Location: index.php');
exit();
