<?php

// Carga variables desde el archivo .env
require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Datos de conexión a la BD
$host = $_ENV['DB_HOST'];
$port = $_ENV['DB_PORT'];
$dbname = $_ENV['DB_NAME'];
$user = $_ENV['DB_USER'];
$password = $_ENV['DB_PASSWORD'];

// DSN para la conexión PDO
$dsn = "pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password";

try {
    // Intentar establecer la conexión a la BD
    $pdo = new PDO($dsn);

    // Configura PDO para excepciones en caso de error
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Consulta SQL
    $sql = "SELECT * FROM productos";

    // Ejecuta la consulta
    $result = $pdo->query($sql);

    // Muestra resultados en pantalla
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        echo "ID: {$row['id']}, Nombre: {$row['nombre']}, Precio: {$row['precio']}\n";
    }
} catch (PDOException $e) {
    // Captura excepciones y muestra mensaje de error
    echo "No se pudo conectar a la base de datos: " . $e->getMessage();
}

// Cierra conexion a la BD
$pdo = null;


