<?php
/*Los namespaces en PHP pueden ayudar a organizar y estructurar el código 
cuando se trabaja con clases y evitan posibles conflictos de nombres. */

// Defino el primer namespace
namespace MiApp\Clases;

// Defino una clase en el namespace
class MiClase {
    public function saludar() {
        echo "Hola con MiApp\Clases\MiClase\n";
    }
}

// Defino otro namespace
namespace OtraApp\Clases;

// Defino una clase en el namespace
class MiClase2 {
    public function despedir() {
        echo "Chau con OtraApp\Clases\MiClase2\n";
    }
}

// Importa el namespace y la clase
use MiApp\Clases\MiClase;

// Crea una instancia de MiClase
$miObjeto = new MiClase();

// Llamada al método saludar
$miObjeto->saludar();

// Importa el namespace y la clase
use OtraApp\Clases\MiClase2;

// Crea una instancia de MiClase2
$otroObjeto = new MiClase2();

// Llamar al método despedir
$otroObjeto->despedir();
