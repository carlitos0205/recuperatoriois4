-- Crear tabla de usuarios
CREATE TABLE IF NOT EXISTS usuarios (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    usuario VARCHAR(50) NOT NULL,
    contrasena VARCHAR(255) NOT NULL
);

-- Inserta usuario de ejemplo
INSERT INTO usuarios (nombre, apellido, usuario, contrasena)
VALUES ('Carlos', 'Arzamendia', 'carza', 'prueba123');
