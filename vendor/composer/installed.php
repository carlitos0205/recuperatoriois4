<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '75f60390397f0f079908d9fa27f32abe5c55d79c',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '75f60390397f0f079908d9fa27f32abe5c55d79c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'brick/math' => array(
            'pretty_version' => '0.11.0',
            'version' => '0.11.0.0',
            'reference' => '0ad82ce168c82ba30d1c01ec86116ab52f589478',
            'type' => 'library',
            'install_path' => __DIR__ . '/../brick/math',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.0.8',
            'version' => '2.0.8.0',
            'reference' => 'f9301a5b2fb1216b2b08f02ba04dc45423db6bff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => 'v1.1.2',
            'version' => '1.1.2.0',
            'reference' => 'fbd48bce38f73f8a4ec8583362e732e4095e5862',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/bus' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => '8fd7ec31c64734ca70f36651b90e8fe4e5b39868',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/bus',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/collections' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => '766a3b6c3e5c8011b037a147266dcf7f93b21223',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/collections',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/conditionable' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'd0958e4741fc9d6f516a552060fd1b829a85e009',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/conditionable',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/container' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'ddc26273085fad3c471b2602ad820e0097ff7939',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/contracts' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'f6bf37a272fda164f6c451407c99f820eb1eb95b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/database' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => '8cebe8112d612a9ce8025a1bce22ca5cc5c1308f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/database',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/events' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => '8d84d6220a6b3446a0bf3e4138e2eb0e10792bb1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/events',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/macroable' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'dff667a46ac37b634dcf68909d9d41e94dc97c27',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/macroable',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/pipeline' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'f2119ae9a26e420bf0ed9d5e7e7aa4548547e7b1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/pipeline',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/support' => array(
            'pretty_version' => 'v10.33.0',
            'version' => '10.33.0.0',
            'reference' => 'f414b40d6149d6a4954f0abceacd1af2edf2d596',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/support',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '2.71.0',
            'version' => '2.71.0.0',
            'reference' => '98276233188583f2ff845a0f992a235472d9466a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.9.2',
            'version' => '1.9.2.0',
            'reference' => '80735db690fe4fc5c76dfa7f9b770634285fa820',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/clock' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'reference' => 'e41a24703d4560fd0acb709162f73b8adfc3aa0d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/clock',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/clock-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.1|2.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.4.0',
            'version' => '3.4.0.0',
            'reference' => '7c3aff79d10325257a001fcf92d991f24fc967cf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v6.3.8',
            'version' => '6.3.8.0',
            'reference' => 'ce332676de1912c4389222987193c3ef38033df6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => 'ea208ce43cbb04af6867b4fdddb1bdbf84cc28cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '42292d99c55abe617799667f454222c54c60e229',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '6caa57379c4aec19c0a12a38b59b26487dcfe4b5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php83' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => 'b0f46ebbeeeda3e9d2faebdfbf4b4eae9b59fa11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php83',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v6.3.7',
            'version' => '6.3.7.0',
            'reference' => '30212e7c87dcb79c83f6362b00bde0e0b1213499',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v3.4.0',
            'version' => '3.4.0.0',
            'reference' => 'dee0c6e5b4c07ce851b462530088e64b255ac9c5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3|3.0',
            ),
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v5.6.0',
            'version' => '5.6.0.0',
            'reference' => '2cf9fb6054c2bb1d59d1f3817706ecdb9d2934c4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'voku/portable-ascii' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
            'type' => 'library',
            'install_path' => __DIR__ . '/../voku/portable-ascii',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
